'''
A simple bank account module
'''

class BankAccount():
    '''
    Class starts here: do some stuff
    '''
    def __init__(self, owner, balance):
        self.owner = owner
        self.balance = balance

    def deposit(self, amount):
        '''
        Get the deposit and update balance
        '''
        self.balance = amount + self.balance
        return f'New Balance is {self.balance}'

    def withdraw(self, amount):
        '''
        Attempt a withdrawal
        '''
        if self.balance > amount:
            self.balance = self.balance - amount
            return f'New Balance is {self.balance}'
        return 'Insufficient Funds'

    def __str__(self):
        return f'Account Owner: {self.owner} \nAccount Balance: {self.balance}'


ACCOUNT = BankAccount('Roy', 100)
print(ACCOUNT)
print(ACCOUNT.deposit(20))
print(ACCOUNT.withdraw(10))
print(ACCOUNT.withdraw(150))
