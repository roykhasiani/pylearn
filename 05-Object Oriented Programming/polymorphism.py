# POLYMORPHISIM: HOW DIFFERENT OBJECT CLASSES CAN SHARE THE SAME SAME METHOD NAME


class Dog():

    def __init__(self, name):
        self.name = name

    def speak(self):
        return self.name + ' says woof!'


class Cat():
    def __init__(self, name):
        self.name = name

    def speak(self):
        return self.name + ' says meow!'


niko = Dog('Niko')
felix = Cat('Felix')

# polymorphism
print(niko.speak())
print(felix.speak())


'''
# demonstrate polymorphism using for loop
for pet in [niko, felix]:
    print(type(pet))
    print(pet.speak())

# demonstrate using a function
def pet_speak(pet):
    print(pet.speak())
pet_speak(felix)  # Felix says meow
pet_speak(niko)  # Niko says woof
'''


# INSTEAD OF POLYMORPHISM, A MORE COMMON APPROACH IS TO USE AN ABSTRACT CLASS:
# ONE THAT NEVER EXPECTS TO BE INSTANTIATED BUT ONLY SERVES AS A BASE CLASS

class Animal():

    def __init__(self, name):
        self.name = name
    
    def speak(self):
        raise NotImplementedError('Sub-class must implement this abstract method')

class Cow(Animal):

    def speak(self):
        return self.name + ' Says MOOOHH'

class Kuku(Animal):

    def speak(self):
        return self.name + ' Says Kwekwekwe'
