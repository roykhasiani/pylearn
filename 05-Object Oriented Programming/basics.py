'''
classes use the CamelCase notation in their definition unlike methods.

'''

class NameOfClass():
    # this is the method used to define an instance of this class (Constructor)
    def __init__(self, param1, param2):
        # use self.args to link the object created to the parameters passed in the method definition and not some 
        # global variable. 
        self.param1 = param1
        self.param2 = param2

    # all other methods take the key-word 'self' to tell python that they belong to the particular class they've been
    # defined under.
    def some_method(self):
        # perform some action
        print(self.param1)