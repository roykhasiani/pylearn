'''
More on class object attributes etc.
'''
#---------------------------START DOG() CLASS------------------------------------
class Dog():

    # class object attribute: Those attribute that every instance of the class must have e.g. a dog is a mammal
    family = 'mammal'

    # call this method any time you create an instance of this class
    def __init__(self, breed, name):
        self.breed = breed
        self.name = name

    def bark(self, number):
        print(f'Woof greetings, my name is {self.name} and my  number is {number}')


# ---------------------------END DOG() CLASS-----------------------------------


my_dog = Dog(breed='German Shepherd', name='Brad')
her_dog = Dog(breed='Huskie', name='Trav')

# print(my_dog.breed)  # German Shepherd
# print(her_dog.family)  # mammal

# call to a method is different from call to an attribute:
# her_dog.bark(9615)  

# ----------------------------------START CLASS CIRCLE()------------------------------
class Circle():
    pi = 3.142

    def __init__(self, radius=1):
        self.radius = radius
        self.area = self.pi * radius* radius  # self.pi = Circle.pi

    def get_circumference(self):
        return Circle.pi*self.radius*2

    def get_area(self):
        return Circle.pi*self.radius**2

    def get_perimeter(self):
        return (Circle.pi*self.radius*2)+self.radius*2

#-----------------------------------END CLASS CIRCLE()--------------------------------

my_circle = Circle(30)
circ = my_circle.get_circumference()
print(my_circle.area)