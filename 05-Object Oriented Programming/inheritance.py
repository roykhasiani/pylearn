# INHERITANCE


# ------------------------------------START OF CLASS ANIMAL()----------------------------------------

class Animal():
    
    def __init__(self):
        print('Animal Created')

    def who_am_i(self):
        print('I am Animal')

    def eat(self):
        print('I am Eating')

#-------------------------------------END OF CLASS ANIMAL()------------------------------------------



# ------------------------------------START OF CLASS DOG()----------------------------------------

class Dog(Animal):  # inherits from Animal()

    def __init__(self):  # creates a dog object
        Animal.__init__(self)  # creates an animal object
        print('Dog Created')

    def who_am_i(self):  # overwrite parent method
        print('I am a Dog')

    def bark(self):  # add new class-specific method
        print('Woof!')
        

#-------------------------------------END OF CLASS ANIMAL()------------------------------------------
dog = Dog()
#dog.eat()
dog.who_am_i()
dog.bark()

# ------------------------------------START OF CLASS ANIMAL()----------------------------------------

#-------------------------------------END OF CLASS ANIMAL()------------------------------------------



# ------------------------------------START OF CLASS ANIMAL()----------------------------------------

#-------------------------------------END OF CLASS ANIMAL()------------------------------------------



# ------------------------------------START OF CLASS ANIMAL()----------------------------------------

#-------------------------------------END OF CLASS ANIMAL()------------------------------------------



# ------------------------------------START OF CLASS ANIMAL()----------------------------------------

#-------------------------------------END OF CLASS ANIMAL()------------------------------------------



# ------------------------------------START OF CLASS ANIMAL()----------------------------------------

#-------------------------------------END OF CLASS ANIMAL()------------------------------------------