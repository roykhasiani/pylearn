class Dog():

    # call this method any time you create an instance of this class
    
    def __init__(self, breed, name, spots):
        self.breed = breed
        self.name = name
        self.spots = spots


# create an object of Sample()

my_dog = Dog(breed='German Shepherd', name='Brad', spots=True)
her_dog = Dog(breed='Huskie', name='Trav', spots=False)

print(my_dog.breed)  # German Shepherd
print(her_dog.breed)  # Lab
print(her_dog.name)  # Trav
print(her_dog.spots)  # False