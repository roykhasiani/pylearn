# HOW CAN WE USE BUILT IN FUNCTIONS WITH OUR OWN OBJECTS EG LEN() AND PRINT()
# USE SPECIAL/ MAGIC/ DUNDER METHODS

class Book():

    def __init__(self, title, author, pages):
        self.title = title
        self.author = author
        self.pages = pages
    
    # what to do if the class is asked for a string representation of it's object
    def __str__(self):
        return f'{self.title} by {self.author}'

    def __len__(self):
        return self.pages

    def __del__(self):
        print('A book object has been deleted')

book = Book('PyConda', 'Roy', 345)
print(book)  # execute __str__(self): PyConda by Roy
print(len(book))  # execute __len__(self): 345
del book
