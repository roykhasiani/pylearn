# if, elif, else for control flow

loc = 'Bank'

if loc == 'Spare parts':
    print('Cars are cool')
elif loc == 'Bank':
    print('Money is stored')
elif loc == 'Nevertheless':
    print('This is love')
else:
    print('I\'m in the dark!!')