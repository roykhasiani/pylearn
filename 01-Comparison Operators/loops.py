# iterable means you can loop through the objects/ values therein e.g.
# dictionary keys are iterable, list elements are iterable, string characters are iterable

# iterate through a list using a for loop (SYNTAX OF FOR LOOP)
my_iterable = [1, 2, 3, 4, 5]
for item in my_iterable:
    print(item)  # or print('hello')
    print('hey')  # hey*5


# combining loops and conditionals
my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for num in my_list:
    if num % 2 == 0:
        print(f'Even number {num}')
    else:
        print(f'Odd number {num}')


# sum up elements in my_list
list_sum = 0
for num in my_list:
    list_sum = list_sum + num
print(list_sum)  # 55


# iterate through strings
for _ in 'Hello':
    print('Love')


# iterate through tuple
tup = (1, 2, 3)
for _ in tup:
    print(_)
# tuples have a special quality in loops (TUPLE UNPACKING):
tup_list = [(1, 2), (3, 4), (5, 6), (7, 8)]
for (a, b) in tup_list:
    print(a)  # now we have access to each item inside each tuple in the list : 1, 3, 5, 7
tup_list2 = [(1, 2, 3), (4, 5, 6), (7, 8, 9)]
for (a, b, c) in tup_list2:
    print(c)  # 3, 6, 9


# iterate through a dictionary
d = {'k1': 1, 'k2': 2, 'k3': 3}
# iterate through the keys only
for _ in d:
    print(_)
# iterate through both the keys and values
for _ in d.items():
    print(_)  # returns tuple pairs
# since it returns tuple pairs, we can use TUPLE UNPACKING HERE TO GET VALUES ONLY:
for (key, value) in d.items():
    print(value)  # 1, 2, 3
# we can also extract values only:
for _ in d.values():
    print(_)  # 1, 2, 3


# WHILE LOOPS
# while loops run given a certain condition remains True
# while loops can be used with else
# example below:

x = 0
while x < 5:
    print(f'The value of x is {x}')
    x += 1
else:  # x is five already so the print below returns 5 in place of {x}
    print(f'The value of x cannot go to {x}')

# Key Words with loops: pass, continue and break

# pass means 'Do nothing at all'
# ---> used to prevent incomplete methods, etc from returning errors
my_string = 'Khas'
for _ in my_string:
    pass  # return nothing

# continue means go back to the top of the closest enclosing loop
for _ in my_string:
    if _ == 'a':
        continue
    print(_)  # Khs

# break breaks out of the current closest enclosing loop
for _ in my_string:
    if _ == 'a':
        break
    print(_)  # Kh




