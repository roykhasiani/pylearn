# nesting comparison operators using and, or, not

# without using and
print(1 < 2 < 3)  # True

# using and
print(1 < 2 and 2 < 3)  # True

# using or
print(1 < 2 or 2 < 3)  # True

# 'not' returns the opposite of the expected bool
print(not(1 < 2 or 2 < 3))
