# Use for, .split(), and if to create a Statement that will print out words that start with 's':
st = 'Print only the words that start with s in this sentence'
st_list = st.split()  # convert it to a list first
for _ in st_list:  # loop through the list
    if _[0] == 's':
        print(' ' + _)

# Use range() to print all the even numbers from 0 to 10.
# numbers = [_ for _ in range(0, 11) if _ % 2 == 0]
# also, cast the range() from 0 to 11 with a step of 2
print(list(range(0, 11, 2)))


# Use a List Comprehension to create a list of all numbers between 1 and 50 that are divisible by 3.
num_list = [_ for _ in range(2, 50) if _ % 3 == 0]
print(num_list)

# Go through the string below and if the length of a word is even print "even!"
st2 = 'Print every word in this sentence that has an even number of letters'
st2_list = st2.split()
for _ in st2_list:
    if len(_) % 2 == 0:
        print('even!')
    else:
        print(_)

'''
Write a program that prints the integers from 1 to 100. But for multiples of three print "Fizz" instead of the number,
 and for the multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz".
'''
raw_fizz_buzz = [_ for _ in range(1, 101)]
for _ in raw_fizz_buzz:
    if _ % 3 == 0 and _ % 5 != 0:
        print(f'Fizz {_}')
    elif _ % 5 == 0 and _ % 3 != 0:
        print(f'Buzz {_}')
    elif _ % 3 == 0 and _ % 5 == 0:
        print(f'FizzBuzz {_}')
    else:
        print(_)

# Use List Comprehension to create a list of the first letters of every word in the string below:
st3 = 'Create a list of the first letters of every word in this string'
st3_list = st3.split()
st3_list_comp = [_[0] for _ in st3_list]
print(st3_list_comp)

