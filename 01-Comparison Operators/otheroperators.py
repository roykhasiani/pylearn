# those that do not fit in the other operators normaly

# range()
for _ in range(10):
    print(_)  # print numbers from 0 up to but not including 10

# range() takes a start, stop, and step where start and step are technically optional
for _ in range(3, 10, 2):
    print(_)  # 3, 5, 7, 9

# range() is a generator and hence can be cast to a list to generate it and make it more efficient
# efficient because it does not save it to memory.
my_list = list(range(0, 8, 2))
print(my_list)

# enumerate() returns tuples from an iterable
word = 'abcde'
for index, letter in enumerate(word):
    print(f'Index {index} contains letter {letter}\n')  # tuple unpacking

# zip() pairs up two or more lists merging one as the indexes of the other
# also returns a list of tuples ready for unpacking
list1 = [0, 1, 2]
list2 = ['a', 'b', 'c']
for l1, l2 in zip(list1, list2):
    print(f'Index {l1} corresponds to Value {l2}')

# in returns True or False if a value is in another iterable or object type
print('a' in 'My friend')  # False

# min(list_mine) returns the minimum value in list_mine
# max(list_mine) returns the maximum value in list_mine

# user input is taken by input(). NOTE: It always picks the input as a string. You can cast it: str(), float(),int().
fav_num = int(input('Favourite number'))
print(f'Fav is {fav_num + 1}')