def number_game(x, y):
    list_range = [_ for _ in range(x, y)]
    if x > y:
        list_range = [_ for _ in range(y, x)]
        new_list = [_ for _ in list_range if _ % 2 == 0]
        print(new_list[1::])
    else:
        new_list = [_ for _ in list_range if _ % 2 != 0]
        print(new_list[1::])


number_game(0, 12)
