# are unique ways of creating a list in python really quick

# old fashioned way (using a for loop) :
my_string = 'hello me'
my_list = []
for _ in my_string:
    my_list.append(_)
print(my_list)

# more efficient using list comprehension
eff_list = [_ for _ in my_string]
print(eff_list)

# example 2:
another_list = [_ for _ in 'Love Lead']
print(another_list)

# example 3:
range_list = [_ for _ in range(2, 5)]
print(range_list)

# example 4: return the squares of even numbers using list comprehensions
range_list2 = [_**2 for _ in range(2, 8) if _ % 2 == 0]
print(range_list2)
