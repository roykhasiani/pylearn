'''
Digital Cypher
'''
import string

def decode(code, key):
    alphabet = string.ascii_lowercase
    alpha_list = list(alphabet)
    code_range = range(1, 27)
    alpha_dict = dict(zip(code_range, alpha_list))
    code_list = []
    error = False
    for item in code:
        if 0 < item < 27:
            code_list.append(alpha_dict[item])
    else:
        error = True
    if error == False:
        print(''.join(code_list))
    else:
        print('Invalid number found')


decode([24, 35, 1], 2)