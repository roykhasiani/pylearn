# sets are unordered collections of unique elements
# no repetitions of similar elements in the same set
#f uncions include add(),

my_set = set()
my_set.add(1)
my_set.add(2)

# the above assignment is slow
# the better approach is to create a list and cast it to a set. see below

my_list = [1, 1, 3, 1, 2, 2, 3, 3, 4, 3, 4]
new_set = set(my_list)
print(my_list)
print(new_set)  # leaves out repeated elements and returns a set version of the list which is sorted in asc

