# FUNCTIONS TO MANIPULATE STRINGS
# Also, indexing and slicing is possible in strings

string_phrase = "Giraffe Academy"
list_p  = [1, 2, 3, 4, 5]
# return value at a certain index
print(string_phrase[2])  # r


# SLICING: [start:stop:step]

# return value from given index up to the end (slicing); 2 = start
print(list_p[1:-1:])  # raf Academy


# return value from given index up to BUT not including another index; 5 = stop
print(string_phrase[2:5:])  # raf

# return value from given index up to BUT not including another index and skip 'x' values after each returned index
# [2::2] would now return from index 2 to the end skipping 1 element and
# [::2] would start from the beginning
print(string_phrase[2:5:2]) #rf

print(string_phrase.lower())  # giraffe academy
print(string_phrase.upper())  # GIRAFFE ACADEMY

print(string_phrase.isupper())  # True
print(string_phrase.islower())  # False

# length of the string (how many characters)
print(len(string_phrase))  # 15

# print a part of the string
print(string_phrase[0])  # G

