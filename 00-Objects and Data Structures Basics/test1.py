# Objects and Data Structures Assessment Test

# Write an equation that uses multiplication, division, an exponent, addition, and subtraction that is equal to 100.25.
print('A)', (0.5*0.5)+(((22/2)-1)**2))


# Answer these 3 questions without typing code. Then type code to check your answer.
# What is the value of the expression 4 * (6 + 5)
# What is the value of the expression 4 * 6 + 5
# What is the value of the expression 4 + 6 * 5
print('B)', 44, '\n  ', 29, '\n  ', 34)

# What is the type of the result of the expression 3 + 1.5 + 4?
print('C)', type(3+1.5+4))

# What would you use to find a number’s square root, as well as its square?
print('D) Square Root = x**.5 and Square = x**2')



# Strings
# Given the string 'hello' give an index command that returns 'e'. Enter your code in the cell below:
my_string = 'hello'
print('E)', my_string[1])

# Reverse the string 'hello' using slicing:
print('F)', my_string[::-1])

# Given the string hello, give two methods of producing the letter 'o' using indexing.
print('G) my_string[-1] and my_string[4]')



# Lists
# Build this list [0,0,0] two separate ways.
my_list = [0, 0, 0]
another_list = [0]*3
print('H)', my_list, another_list)

# Reassign 'hello' in this nested list to say 'goodbye' instead:
list3 = [1, 2, [3, 4, 'hello']]
list3[2][2] = 'goodbye'
print('I)', list3)

# Sort the list below:
list4 = [5, 3, 4, 6, 1]
list4.sort()
print('J)', list4)

# Dictionaries
# Using keys and indexing, grab the 'hello' from the following dictionaries:
print('K)Answers as follows:')
d = {'simple_key': 'hello'}
print('\t(1)', d['simple_key'])
d = {'k1': {'k2': 'hello'}}
print('\t(2)', d['k1']['k2'])
d = {'k1': [{'nest_key': ['this is deep', ['hello']]}]}
print('\t(3)', d['k1'][0]['nest_key'][1][0])
d = {'k1': [1, 2, {'k2': ['this is tricky', {'tough': [1, 2, ['hello']]}]}]}
print('\t(4)', d['k1'][2]['k2'][1]['tough'][2][0])



# Sets
# What is unique about a set?
# Use a set to find the unique values of the list below:
list5 = [1, 2, 2, 33, 4, 4, 11, 22, 3, 3, 2]
unique_values = set(list5)
print('L', unique_values)


