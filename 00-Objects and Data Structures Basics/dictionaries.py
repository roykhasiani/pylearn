# unordered key:value pairs
# cannot be sorted
# when to choose between lists and dictionaries?
# use dictionaries when you wanna retrieve a value without knowing its exact index location but you know its key
# dictionaries use curly braces
# dictionaries can also hold lists or other dictionaries

my_dictionary = {'key1': 'value1', 'key2': 'value2'}
print(my_dictionary['key1'])  # value1

prices = {'apple': 2.99, 'orange': 1.99, 'mango': 0.99}
print(prices['orange'])

# dictionary with items, a list and another dictionary
vast_dictionary = {'t-shirt': 20, 'trouser': 50, 'customers': ["Roy", "Murwa", "Khasiani"],
                   'tickets': {"jay-z": 100, "j-cole": 99}}

# return values from the above dictionary by stacking index calls i.e.
# if we want to get the second customer name (Murwa):
print(vast_dictionary['customers'][1])

# if we want to get the ticket price for a j-cole concert ($99)
print(vast_dictionary['tickets']['j-cole'])

# knowing the return value type, i.e. str, list, dict, we can apply various methods:
print(vast_dictionary['customers'][0].upper())  # ROY

# lets reassign a value to a given key
my_dictionary['key1'] = 'new value'
print(my_dictionary['key1'])

# see all the keys of a dictionary
print(vast_dictionary.keys())

# see all the values of a dictionary
print(vast_dictionary.values())

# see all the items in a dictionary
print(vast_dictionary.items())
