character_name = "John"
character_age = "70"

print("There was once a man named " + character_name + ".")
print("John was " + character_age + " years old.")

# update variable mid-execution
character_name = "Mike"

print("He really loved the name " + character_name + ".")
print("However, he did not like being " + character_age + " years old.")
