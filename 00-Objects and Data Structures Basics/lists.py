# this is a comment
# lists are ordered mutable sequence of objects
# common functions in lists are sort(), reverse(), append(), pop()
# we can also slice or index lists to extract elements without changing the original list
# slicing and indexing are commonly used with strings too.
# sort(), append() append() and insert() change the list inline (permanently) without returning anything to assign

letter_list = ['a', 'b', 'c', 'd', 'e']

# check the type of object or data structure
# print(type(num_list))

# add an element to the end of the list
letter_list.append('f')
print('Added \'f\' to the end of the list: ')
print(letter_list)

# replace an element at an index(like insert)
letter_list[0] = 'f'
print('Insert \'f\' at beginning: ')
print(letter_list)

# remove an element from the last index of the list
letter_list.pop()
print('Popped last index: ')
print(letter_list)

# remove an element from a specific index
letter_list.pop(2)
print('Popped value at index [2]: ')
print(letter_list)

# sort in ascending order
letter_list.sort()
print('Sorted in asc: ')
print(letter_list)

# sort in descending order
letter_list.reverse()
print('Sorted in desc: ')
print(letter_list)

# NB: THE LIST IS NOW PERMANENTLY CHANGED TO THE FINAL LIST VALUE AT LINE 30
