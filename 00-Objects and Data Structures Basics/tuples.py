# Tuples are immutable ordered sequence of objects
# use parentheses unlike square braces as in lists
# let's compare tuples and lists
# tuples have only two inbuilt functions: index() and count()


my_list = ['one', 2, 3]
my_tuple = ('one', 2, 3)

# print(type(my_tuple))

# check length as in strings or lists using the same function
print(len(my_tuple))

# count() returns number of times an element occurs in the tuple
t = ('a', 'a', 'b')
print(t.count('a'))

# index() returns the first index that a repeated or non repeated element occurs
print(t.index('a'))  # the first time 'a' appears is at index 0 so 0 is returned
print(t.index('b'))  # 2

# tuples do not allow item reassignment
# e.g. t[0] = c  returns an error but my_list[0] = 1 returns [1, 2, 3]
