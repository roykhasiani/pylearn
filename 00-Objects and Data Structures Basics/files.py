# create a txt file
# file functions include read(), readlines(), open(), close(), seek(0)

f = open('myfile.txt', 'w+')

# data to our file
f.write('Hello, this is my first line \nThis is the second line \nThis is the third')

# return whatever has been written in our file
f.seek(0)  # seek the cursor back to the beginning of the file
print(f.read())

# we want to parse each new line in the .txt file into a list with each line corresponding to a single element
# seek the cursor to the beginning again
f.seek(0)
print(f.readlines())

# read() grabs everything as a string while readlines() grabs everything as a list

# FILE PATHS
# to open a file in a different location than your current directory, simply pass the full file path to open()
# for example,
# f = open('C:\\users\\mine\\desktop\\myfile.txt')
# use double backslashes to tell python to interpret it as is

# always close file in order to avoid delete or move errors since python keeps using it
# f.close()

# HOWEVER, the BEST PRACTICE to avoid having to worry about closing files each time is to open a file as follows

with open('newfile.txt', 'w+') as my_new_file:
    my_new_file.write('The quick\nBrown fox\nJumps over\nThe lazy dog')
    my_new_file.seek(0)
    print(my_new_file.read())
    my_new_file.seek(0)
    print(my_new_file.readlines())