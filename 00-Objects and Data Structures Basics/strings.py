# WORKING WITH STRINGS
# strings are ordered sequence of characters

# simple string
print("Giraffe Academy")

# new line
print("Giraffe \n Academy")

# quotation marks(for double, just replace the singles)
print("Giraffe \'Academy\'")

# just a backslash
print("Giraffe \ Academy")

# store a string in a variable
string_phrase = "Giraffe Academy"
print(string_phrase) # Giraffe Academy

# string concatenation
print(string_phrase + " is cool")
