'''
Lambda Expressions
Map
Filter
'''


def square(num):
    return num**2


my_nums = [1, 2, 3, 4, 5]

'''
imagine we want to apply the square function to all the numbers in the list my_nums above
a for loop would take too much code and time
a map would be handy
loop through it to apply it to every item in an iterable
simply pass the function that you want to map to every element in the iterable
expects a function and an iterable
'''

for item in map(square, my_nums):  # squares every element in my_nums list
    print(item)


# map with a function that returns strings
def splicer(st):
    if len(st) % 2 == 0:
        return 'Even'
    else:
        return st[0]


names = ['Andy', 'Eve', 'Sally']

# for spliced in map(splicer, names):
#     print(spliced )

print(list(map(splicer, names)))