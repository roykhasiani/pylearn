'''
*args means as many arguments as possible
**kwargs means key-word arguments:  builds a dictionary of key : value pairs
'''


def myfunc(a, b):
    # returns 5% of the sum of a and b
    return sum(a, b)*.05
    # what if we want to add more positional arguments to myfunc() to work with?
    # 1) we could just assign more parameters and set their defaults to 0 eg myfunc(a, b, c=0, d=0,...)
    # 2) we SHOULD use " *args " inside the method parentheses in order to allow for unlimited values:


# BEST PRACTICE *ARGS:
def myfunc2(*args):
    return sum(args) * .05


# BEST PRACTICE **KWARGS:
def myfunct3(**kwargs):
    if 'fruit' in kwargs:
        print('My fruit of choice is {}'.format(kwargs['fruit']))
        print(f"My fruit of choice is {kwargs['fruit']}")  # same with line above
    else:
        print('I didn\'t find any fruit here')


# now we can pass any key words to the function:
myfunct3(fruit='apple', veggie='sukuma wiki')


# COMBINED: USEFUL WHEN USING THIRD-PARTY LIBRARIES:
def myfunct4(*args, **kwargs):
    if 'food' in kwargs and 'soda' in args:
        print(f'I will have {kwargs["food"]} and {args[0]} please')


print(myfunct4('soda', food='Fries'))
