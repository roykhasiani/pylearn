'''
Functions and Methods Homework
Complete the following questions:

Write a function that computes the volume of a sphere given its radius.

The volume of a sphere is given as ((4/3)π(r)^3)
'''

import math
import numpy
import string


def vol(rad):
    return math.pi * (4/3) * (rad**3)


'''
Write a function that checks whether a number is in a given range (inclusive of high and low)
'''

def run_check(num, low, high):
    if num in range(low, high+1):
        print('Present')
    else:
        print('Absent')


'''
Write a Python function that accepts a string and calculates the number of upper case letters and lower case letters.

Sample String : 'Hello Mr. Rogers, how are you this fine Tuesday?'
Expected Output : 
No. of Upper case characters : 4
No. of Lower case Characters : 33

HINT: Two string methods that might prove useful: .isupper() and .islower()

If you feel ambitious, explore the Collections module to solve this problem!
'''


def up_low(st):
    upper_list = []
    lower_list = []
    for letter in list(st):
        if letter.isupper():
            upper_list.append(letter)
        elif letter.islower():
            lower_list.append(letter)
    print(f'No. of Upper case characters : {len(upper_list)}')
    print(f'No. of Lower case Characters : {len(lower_list)}')



'''
Write a Python function that takes a list and returns a new list with unique elements of the first list.

Sample List : [1,1,1,1,2,2,3,3,3,3,4,5]
Unique List : [1, 2, 3, 4, 5]
'''

def unique_list(lst):
    print(list(set(lst)))



'''
Write a Python function to multiply all the numbers in a list.

Sample List : [1, 2, 3, -4]
Expected Output : -24
'''

def multiply(numbers):
    return numpy.prod(numbers)


'''
Write a Python function that checks whether a passed in string is palindrome or not.

Note: A palindrome is word, phrase, or sequence that reads the same backward as forward, e.g., madam or nurses run.
'''

def palindrome(s):
    if s == s[::-1]:
        return True
    else:
        return False


'''
Hard:
Write a Python function to check whether a string is pangram or not.

Note : Pangrams are words or sentences containing every letter of the alphabet at least once.
For example : "The quick brown fox jumps over the lazy dog"

Hint: Look at the string module
'''

def ispangram(str1, alphabet=string.ascii_lowercase):
    if all(x in str1 for x in alphabet):
        return True
    else:
        return False


