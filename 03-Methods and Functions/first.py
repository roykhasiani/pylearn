''' DOCSTRING:

my_list = [1, 2, 3, 4, 5]
print(my_list)
my_list.clear()
print(my_list)
# check documentation for a builtin function in python
# help(my_list.clear)

'''


# define a simple function
def name_function():
    '''
    DOCSTRING: Information about the function
    :input: no input
    :output: prints 'Hello World'
    :return: no return
    '''
    print('Hello World')


name_function()  # Hello World


# a function that requires takes an argument
def name_func(name):
    print(f'My name is {name}')


# to avoid an error being thrown whenever we do not pass an argument,
# we set a DEFAULT value to the function parameter by introducing '=': see below
def func_name(name='Name not set'):
    print(f'My name is {name}')


# use return in order to be able to assign the value returned by a method and reuse it elsewhere:
def add_function(n1=0, n2=0):  # set defaults for n1 and n2 in case they are not provided
    return n1 + n2


# assign returned value to 'summation'
summation1 = add_function()
summation2 = add_function(2)
summation3 = add_function(2, 3)
print(summation1)  # 0
print(summation2)  # 2
print(summation3)  # 3


# Find out if a string being looked for is in a string
def word_search(dog_string):
    return 'dog' in dog_string.lower()  # boolean: returns True or False


result = word_search('mydog is here')
print(result)  # True


# Pig Latin translator:
# if a word starts with a vowel add 'ay' in the end
# if not take the first letter of the word, append it to the end and add 'ay' at the end too
def pig_latin(word):
    if word[0] in 'aeiou':
        pig_word = word + 'ay'
    else:
        pig_word = word[1::] + word[0] + 'ay'

    return pig_word


print(pig_latin('word'))