'''
Filter by a function that returns True or False
'''


def check_even(num):
    return num % 2 == 0


numbers = [1, 2, 3, 4, 5, 6]

# Filter elements from iterable based on the CONDITION of the function passed
print(list(filter(check_even, numbers)))  # return them as a list
for number in filter(check_even, numbers):  # return them as individual values
    print(number)