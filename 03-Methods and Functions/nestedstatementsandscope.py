# Variables are stored in a namespace in the project
# LEGB Rules for Scope: Local, Enclosing function locals, Global, Built-in
# The order that Python looks for scope for variables when called

# GLOBAL
name = 'THIS IS A GLOBAL VARIABLE'


def greet():

    # ENCLOSING
    name = 'Sammy'

    def hello():

        # LOCAL
        global name
        name = 'Fred'
        print(f'Hello {name}')

    hello()


greet()  # Hello Sammy


# How can we alter / reassign a global variable?
# See below:

st = 'Roy'

# don't accept st in function definition parentheses
def greeting():
    # call the global variable
    global st
    # from here, any reassignment of st alters the global st eg:
    st = 'HumTwice'
    print(st)


greeting()  # Humtwice
print(st)  # Humtwice since the global has been changed within greeting()