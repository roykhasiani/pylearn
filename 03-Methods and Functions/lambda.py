'''
Convert a function into a lambda expression(a.k.a an anonymous function)
'''


# turn the function below into a lambda expression
def square(num):
    result = num ** 2
    return result


# can also be:
def square(num):
    return num ** 2


# can also be:
def square(num): return num ** 2


# finally the lambda expression
# get rid of def, name of function(square) and return:
nums = [1, 2, 3, 4, 5, 6]
print(list(map(lambda num: num ** 2, nums)))

# lambda to manipulate a string
st = ['Andy', 'Eve', 'Sally']
print(list(map(lambda x: x[::-1], st)))  # ['ydnA', 'evE', 'yllaS']
print(list(map(lambda x: x[0], st)))  # ['A', 'E', 'S']
