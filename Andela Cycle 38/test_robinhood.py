import unittest
import robinhood

class TestRobinHood(unittest.TestCase):
    '''
    Only one test
    '''
    def test_robinhood(self):
        self.assertFalse(robinhood.hero(2, 2))
        self.assertTrue(robinhood.hero(4, 2))
        self.assertTrue(robinhood.hero(7, 3))
        self.assertFalse(robinhood.hero(1, 1))


if __name__ == '__main__':
    unittest.main()