'''
Nearest Elevator
Zero pycodestyle pep8 issues. -:)
'''

def elevator(left, right, call):
    '''
    Parameters:
        left :
            The current floor of the left elevator
        right:
            The current floor of the right elevator
        call:
            The called floor, where you want to reach
    Assume:
        The passed params are integers from 0 to 2
    Return:
        Name of elevator closest to called floor,
        i.e elevators(0, 1, 0) ==> "left"
    '''
    if call == left and call != right:
        return 'left'
    elif call == right and call != left:
        return 'right'
    elif call == right == left:
        return 'right'
    elif abs(call - left) > abs(call - right):
        return 'right'
    elif abs(call - left) == abs(call - right):
        return 'right'


# Tests alternative:
#print(elevator(0, 1, 0))  # => "left"
#print(elevator(0, 1, 1))  # => "right"
#print(elevator(0, 1, 2))  # => "right"
#print(elevator(0, 0, 0))  # => "right"
#print(elevator(0, 2, 1))  # => "right"
