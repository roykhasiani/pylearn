import unittest
from ahoy import Ship


class TestAhoy(unittest.TestCase):

    def test_ahoy(self):
        titanic_1 = Ship(35, 10)
        titanic_2 = Ship(20, 0)
        titanic_3 = Ship(35, 6)
        titanic_4 = Ship(20, 10)
        self.assertTrue(titanic_1.is_worth_it())
        self.assertTrue(titanic_2.is_worth_it())
        self.assertTrue(titanic_3.is_worth_it())
        self.assertFalse(titanic_4.is_worth_it())


if __name__ == '__main__':
    unittest.main()
