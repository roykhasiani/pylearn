'''
Is a ship worth looting?
'''


class Ship:
    '''
    Start here.
    Assume positive integers passed on instantiation.
    '''
    def __init__(self, draft, crew):
        self.draft = draft
        self.crew = crew

    def is_worth_it(self):
        '''
        Check if there is no crew and graft is greater than 20 units
        Else Get total graft weight due to crew weight and less from graft to get net graft weight
        Return True if net graft weight > 20
        '''
        crew_to_draft = 1.5 * self.crew
        can_loot = True
        if self.draft > 20 and self.crew == 0:
            return can_loot
        elif self.draft - crew_to_draft >= 20:
            return can_loot
        return not can_loot


titanic = Ship(35, 10)  # True
result = titanic.is_worth_it()
print(result)
