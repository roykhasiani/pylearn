import unittest
import elevators

class TestElevators(unittest.TestCase):
    '''
    Check for correct output
    '''
    def test_elevator(self):
        self.assertEqual(elevators.elevator(0, 1, 0), 'left')
        self.assertEqual(elevators.elevator(0, 1, 1), 'right')
        self.assertEqual(elevators.elevator(0, 1, 2), 'right')
        self.assertEqual(elevators.elevator(0, 0, 0), 'right')
        self.assertEqual(elevators.elevator(0, 2, 1), 'right')


if __name__ == '__main__':
    unittest.main()