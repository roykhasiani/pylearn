board = ['#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
user1 = True
user2 = False
turn = ''

inserted_num = 0


def displayboard(board):
    print(board[7] + '|' + board[8] + '|' + board[9])
    # print('-|-|-')
    print(board[4] + '|' + board[5] + '|' + board[6])
    # print('-|-|-')
    print(board[1] + '|' + board[2] + '|' + board[3])


user1_symbol = ''
user2_symbol = ''


def play():
    global turn
    if turn == user1:
        inserted_num = int(
            input(
                'Player 1: Please input a number to place your symbol (1-9)'))
        board[inserted_num] = user1_symbol
        displayboard(board)
        turn = user2
    if turn == user2:
        inserted_num = int(
            input(
                'Player 2: Please input a number to place your symbol (1-9)'))
        board[inserted_num] = user2_symbol
        displayboard(board)
        turn = user1



def choose_symbol():
    player_symbol = input('Please input what you want to play with ("X" or "O")')

    if player_symbol in ['X', 'x']:
        global user1_symbol
        user1_symbol = player_symbol
        global user2_symbol
        user2_symbol = 'O'
        global turn
        turn = user1
        play()

    elif player_symbol in ['O', 'o']:
        user1_symbol = player_symbol
        user2_symbol = 'X'
        turn = user2
        play()

    else:
        print('Invalid Input')

choose_symbol()