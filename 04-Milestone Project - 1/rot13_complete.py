'''
----------developed by @roykhasiani-------------------------
'''

import string


def rot13(message):

    # get a string containing all the alphabet letters in lowercase
    dictionary = string.ascii_lowercase

    # convert the passed string to lowercase to avoid comparison issues
    message = message.lower()

    # convert the above string to a list for easy replacement by the cypher
    list_message = list(message)

    # declare a variable to hold an index for the new letter to be subsituted with
    new_letter_index = 0

    # loop through the list created in line 17
    for letter in list_message:

        # check every letter in the string given against the alphabet
        if letter in dictionary:

            # if it exists, get it's index, add 13 to get the new index for replacement
            new_letter_index = dictionary.index(letter) + 13

            # get the letter  13 indices after our original letter in the string
            # check if the index is out of range to start back at 0
            if new_letter_index > 25:
                new_letter_index = new_letter_index - 26

            new_letter = dictionary[new_letter_index]

            # get the index of the original letter in the list
            old_letter_index = list_message.index(letter)

            # replace it with the new letter
            list_message[old_letter_index] = new_letter

    # join back the list into a string that has been ciphered
    print(''.join(list_message))


# test it
rot13('roy')  # output: ebl
